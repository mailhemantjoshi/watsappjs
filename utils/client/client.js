const { Client } = require('whatsapp-web.js');

exports.createClient = (sessionConfig) => {
  try {
    const client = new Client({
      puppeteer: {
        headless: true,
        executablePath: '/usr/bin/chromium-browser', //  <-only use when you also need to upload videos
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-dev-shm-usage',
          '--disable-accelerated-2d-canvas',
          '--no-first-run',
          '--no-zygote',
          '--single-process',
          '--disable-gpu',
        ],
      },
      session: sessionConfig,
    });
    return client;
  } catch (err) {
    console.log(err);
  }
};
