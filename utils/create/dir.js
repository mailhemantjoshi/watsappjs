const fs = require('fs');
const logRootPath = require('../../logRootPath');
const createDirIfNotExists = () => {
  try {
    if (
      !fs.existsSync(`${logRootPath()}/temp`, (err) => {
        if (err) console.log(err);
      })
    ) {
      fs.mkdirSync(`${logRootPath()}/temp`, (err) => {
        if (err) console.log(err);
      });
      fs.mkdirSync(
        `${logRootPath()}/temp/log/error`,
        { recursive: true },
        (err) => {
          if (err) console.log(err);
        }
      );
      fs.mkdirSync(
        `${logRootPath()}/temp/log/function`,
        { recursive: true },
        (err) => {
          if (err) console.log(err);
        }
      );
      fs.mkdirSync(
        `${logRootPath()}/temp/log/general`,
        { recursive: true },
        (err) => {
          if (err) console.log(err);
        }
      );
      fs.mkdirSync(
        `${logRootPath()}/temp/log/success`,
        { recursive: true },
        (err) => {
          if (err) console.log(err);
        }
      );
      fs.mkdirSync(
        `${logRootPath()}/temp/media`,
        { recursive: true },
        (err) => {
          if (err) console.log(err);
        }
      );
      return true;
    }
  } catch (err) {
    console.log(err);
    return { err };
  }
};

exports.createDirIfNotExists = createDirIfNotExists;
