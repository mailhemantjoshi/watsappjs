const path = require('path');

const dirPath = () => {
  return path.resolve(__dirname);
};

module.exports = { dirPath };
