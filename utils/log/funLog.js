/* ============================================================
  Written By: Hemant Joshi
  File created on: 23 sept 2021
============================================================ */

const fs = require('fs');
const { table } = require('table');
const logRootPath = require('../../logRootPath');
const { fullTime, fullDate } = require('../date');

const writeFun = (funName, user) => {
  let Data = [
    [`Ran Function`, `${funName}`],
    [`By User`, `${user}`],
    [`AT`, `${fullTime()}`],
  ];
  const config = {
    singleLine: true,
  };
  const data = table(Data, config);
  try {
    fs.writeFileSync(
      `${logRootPath()}/temp/log/function/${fullDate()}.txt`,
      '\r\n' + data + '\r\n',
      { flag: 'a+' },
      (err) => {
        console.log(err);
      }
    );
  } catch (err) {
    console.log(err);
  }
};

module.exports = writeFun;
