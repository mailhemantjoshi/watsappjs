/* ============================================================
  Written By: Hemant Joshi
  File created on: 23 sept 2021
============================================================ */

const fs = require('fs');
const logRootPath = require('../../logRootPath');
const { fullTime, fullDate } = require('../date');

const writeUserSuccess = (number, user) => {
  const data = `sent to ${number} [AT: ${fullTime()}]`;
  try {
    fs.writeFileSync(
      `${logRootPath()}/temp/log/success/${user}-${fullDate()}.txt`,
      data + '\r\n',
      { flag: 'a+' },
      (err) => {
        console.log(err);
      }
    );
  } catch (err) {
    console.log(err);
  }
};

module.exports = writeUserSuccess;
