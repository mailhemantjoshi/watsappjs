/* ============================================================
  Written By: Hemant Joshi
  File created on: 23 sept 2021
============================================================ */

const fs = require('fs');
const logRootPath = require('../../logRootPath');
const { fullTime, fullDate } = require('../date');

const writeUserError = (error, user) => {
  const data = `${error} [AT: ${fullTime()}]`;
  try {
    fs.writeFileSync(
      `${logRootPath()}/temp/log/error/${user}-${fullDate()}.txt`,
      '\r\n' + data,
      { flag: 'a+' },
      (err) => {
        console.log(err);
      }
    );
  } catch (err) {
    console.log(err);
  }
};

module.exports = writeUserError;
