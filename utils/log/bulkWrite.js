const writeUserError = require('./userError');
const writeUserSuccess = require('./userSuccess');

const bulkWrite = (arrayOfData, user, status) => {
  try {
    if (status === 'success') {
      for (let i = 0; i < arrayOfData.length; i++) {
        writeUserSuccess(`sent to ${arrayOfData[i]}`, user);
      }
    } else if (status === 'fail') {
      for (let i = 0; i < arrayOfData.length; i++) {
        writeUserError(`Number not found ${arrayOfData[i]}`, user);
      }
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = bulkWrite;
