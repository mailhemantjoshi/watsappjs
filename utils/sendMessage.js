const { MessageMedia } = require('whatsapp-web.js');
const writeUserError = require('./log/userError');
const download = require('image-downloader');
const logRootPath = require('../logRootPath');



const todaysUrl = (event, date) =>{
  const url = `https://d2r2picpvggc2n.cloudfront.net/uttarakhand/uploads/AAP/KAPKOT/${date}/EVENT${event}`;
  return url;
}

// date: 23-11-2021

exports.sendTextMessage = async (client, number, data, userId) => {
  try {
    // console.log(mediaPath);
    console.log(data);
    // if (!data.image && !data.video) {
    //   console.log('ran');
    //   const res = await client.sendMessage(number, data.text);
    //   return res.id.fromMe;
    // } else if (data.image || data.video) {
    //   console.log('ran');
    //   const path = mediaPath;
    //   let media = MessageMedia.fromFilePath(path);
    //   const res = await client.sendMessage(number, media, {
    //     caption: data.text || ' ',
    //   });
    //   return res.id.fromMe;
    // }
    const url = todaysUrl(data.event, data.date);
    let userImageUrl = url + '/' + userId + '.jpg';
    // let userImageUrl = url + userId + '.jpg';
    const option = {
      url: userImageUrl,
      dest: `${logRootPath()}/temp/media/${userId}.jpg`,
    };

    await download.image(option);
    const path = option.dest;
    let media = MessageMedia.fromFilePath(path);
    const res = await client.sendMessage(number, media, {
      caption:' ',
    });
    await client.sendMessage(number, data.text);
    return res.id.fromMe;
  } catch (err) {
    writeUserError('utils/sendMessage' + err, 'general');
    console.log(err);
  }
};
