/* ============================================================
  Written By: Hemant Joshi
  File created on: 15 sept 2021
============================================================ */

/* ============================================================
@currentDate 
Functions to create javascript Date() object
============================================================ */
currentDate = () => new Date();

/* ============================================================
@fullDate 
Functions to return full Date now
============================================================ */
fullDate = () => {
  return `${currentDate().getDate()}-${
    currentDate().getMonth() + 1
  }-${currentDate().getFullYear()}`;
};

/* ============================================================
@fulltime 
Functions to return full time now
============================================================ */
fullTime = () => {
  return `${currentDate().getDate()}/${
    currentDate().getMonth() + 1
  }/${currentDate().getFullYear()} @ ${currentDate().getHours()}:${currentDate().getMinutes()}:${currentDate().getSeconds()} `;
};

module.exports = { currentDate, fullDate, fullTime };
