const XLSX = require('xlsx');
const logRootPath = require('../../logRootPath');

// Functions to return json Object of contacts
const readAndParseXLSX = () => {
  try {
    // Importing the Contact XLSX file from local directory
    const workbook = XLSX.readFile(`${logRootPath()}/test.xlsx`);
    // Get the first worksheet and sheetNames;
    const sheetNameList = workbook.SheetNames;

    // Sheets to json object
    const contactNumber = XLSX.utils.sheet_to_json(
      workbook.Sheets[sheetNameList[0]]
    );
    return contactNumber;
  } catch (err) {
    writeError(
      'Please Check the Contacts.XLSX file exists in right path',
      err,
      'readAndParseXLSX'
    );
    // console.log(err);
    return { error: 'error', err };
  }
};

module.exports = { readAndParseXLSX };

// console.log(contactNumber[0].Numbers);
