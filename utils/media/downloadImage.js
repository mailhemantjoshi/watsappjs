/* ============================================================
  Written By: Hemant Joshi
  File created on: 15 sept 2021
============================================================ */

const axios = require('axios');
const fs = require('fs');
const logRootPath = require('../../logRootPath');
const writeFun = require('../log/funLog');

const downloadImage = async (url, user, fileName) => {
  try {
    await axios({
      url,
      responseType: 'stream',
    })
      .then((res) => {
        if (!fs.existsSync(`${logRootPath()}/temp/media/${user}`)) {
          fs.mkdirSync(`${logRootPath()}/temp/media/${user}`);
        }
        // get data from response....
        res.data
          .pipe(
            fs.createWriteStream(
              `${logRootPath()}/temp/media/${user}/${fileName}`
            )
          )
          .on('finish', () => {})
          .on('error', () => {});

        writeFun(`Downloaded Image from url ${url}`, user);
      })
      .catch((err) => {
        console.log(err);
      });

    return `${logRootPath()}/temp/media/${user}/${fileName}`;
  } catch (err) {
    console.log(err);
  }
};

// downloadImage(
//   `https://images.hindustantimes.com/rf/image_size_630x354/HT/p2/2020/12/17/Pictures/_ccb95a28-4065-11eb-ba42-7bdceb016500.jpg`,
//   'hemant',
//   `send-i.png`
// );

exports.downloadImage = downloadImage;
