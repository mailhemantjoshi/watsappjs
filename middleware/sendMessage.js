const fs = require('fs');
const logRootPath = require('../logRootPath');

exports.sendMessageMiddleware = async (req, res, next) => {
  if (!req.body.text) return res.status(400).send('Please send Proper text');
  if (!req.body.user) return res.status(400).send('Please Enter a Username');
  if (!req.body.event) return res.status(400).send('Please Enter The Event');
  if (req.body.image && req.body.video)
    return res
      .status(400)
      .send(
        'You cannot send multiple media in a single message body, try making separate requests'
      );
  if (req.body.image || req.body.video) {
    if (
      req.body.image &&
      !fs.existsSync(
        `${logRootPath()}/temp/media/${req.body.user}/${req.body.image}`
      )
    ) {
      return res
        .status(400)
        .send(
          `Please check, the file dosent exists at the ${logRootPath()}/temp/media/${
            req.body.user
          }/${req.body.image}`
        );
    }
    if (
      req.body.video &&
      !fs.existsSync(
        `${logRootPath()}/temp/media/${req.body.user}/${req.body.video}`
      )
    ) {
      console.log('No Vid');
      return res
        .status(400)
        .send(
          `Please check, the file dosent exists at the ${logRootPath()}/temp/media/${
            req.body.user
          }/${req.body.video}`
        );
    }
    if (
      req.body.video &&
      fs.existsSync(
        `${logRootPath()}/temp/media/${req.body.user}/${req.body.video}`
      )
    ) {
      console.log('Vid found');
      const fileStat = fs.statSync(
        `${logRootPath()}/temp/media/${req.body.user}/${req.body.video}`
      );
      const fileSizeInBytes = fileStat.size;
      const fileSizeInMB = fileSizeInBytes / 1000000.0;
      if (fileSizeInMB > 15.99) {
        return res
          .status(400)
          .send(
            'Video exceeds 15.99 MB size, media less then 15.99 can be only sent'
          );
      }
    }
  }
  next();
};
