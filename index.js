/* ============================================================
  Written By: Hemant Joshi
  File created on: 22 sept 2021
============================================================ */

const express = require('express');
const sendMessageRouter = require('./routes/sendMessage');
const { createDirIfNotExists } = require('./utils/create/dir');
const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// self invoking function.
(async () => {
  createDirIfNotExists();
  app.use('/api', sendMessageRouter);
  const port = process.argv[2] || 8000;

  app.listen(port, () => {
    console.log(`Running live on ${port}`);
  });
})();
