const { Client } = require('whatsapp-web.js');
const qrcode = require('qrcode-terminal');
const fs = require('fs');

const SESSION_FILE = './whatsappSession.json';
let sessionConfig;

if (fs.existsSync(SESSION_FILE)) {
  sessionConfig = require(SESSION_FILE);
}

const client = new Client({
  puppeteer: { headless: true },
  session: sessionConfig,
});

client.on('qr', (qr) => {
  console.log('QR Code RECEIVED', qr);
  qrcode.generate(qr, { small: true });
});

client.on('authenticated', (session) => {
  console.log('Authenticated');
  sessionConfig = session;
  fs.writeFileSync(SESSION_FILE, JSON.stringify(sessionConfig), (err) => {
    if (err) {
      console.error(err);
    }
  });
});

client.on('ready', () => {
  console.log('Ready to pair up');
});

const phone = '9675691382';
const text = 'Hey john हिन्दी';
// const chatId = number.substring(1) + '@c.us';

client.initialize().then(() => {
  console.log('initialize');
});
// console.log(chatId);

// const sanitized_number = phone.toString().replace(/[- )(]/g, ''); // remove unnecessary chars from the number
// const final_number = `91${sanitized_number.substring(
//   sanitized_number.length - 10
// )}`; // add 91 before the number here 91 is country code of India
// client.getNumberId(final_number).then((resp) => {
//   console.log('number_details', resp);
//   if (resp) {
//     client
//       .sendMessage(resp._serialized, text)
//       .then((response) => {
//         console.log('Message sent', response);
//         if (response.id.fromMe) {
//           res.send({
//             status: 'success',
//             message: `Message successfully sent to ${final_number}`,
//             response,
//             resp,
//           });
//         }
//       })
//       .catch((err) => {
//         console.log(err);
//       });
//   }
// });

// setTimeout(() => {
//   console.log('running');

// }, 9000);
