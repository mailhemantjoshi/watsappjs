const path = require('path');

/* ============================================================
@logPathLog 
Functions to log path of log folder
============================================================ */
const logRootPath = () => {
  return path.resolve(__dirname);
};
module.exports = logRootPath;
