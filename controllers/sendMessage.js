/* ============================================================
  Written By: Hemant Joshi
  File created on: 22 sept 2021
============================================================ */

const fs = require('fs');
const qrcode = require('qrcode-terminal');
const { sendTextMessage } = require('../utils/sendMessage');
const writeFun = require('../utils/log/funLog');
const writeUserError = require('../utils/log/userError');
const writeSuccess = require('../utils/log/success');
const logRootPath = require('../logRootPath');
const { downloadImage } = require('../utils/media/downloadImage');
const { createClient } = require('../utils/client/client');
const bulkWrite = require('../utils/log/bulkWrite');
const { readAndParseXLSX } = require('../utils/parseXlsx/parseXl');
const writeUserSuccess = require('../utils/log/userSuccess');

/* ============================================================
  Send Message Controller method
  @param {object} req [express.request object]
  @param {object} res [express.response object]
============================================================ */

exports.sendMessageController = async (req, res) => {
  // get user, message text and image if any from the request's body. [typeof req.body == JSON ]
  const { user, text, image, video, event, date } = req.body;
  // try catch block to handle errors
  try {
    // create the Session file name for current username ex: user1-whatsappSession.json
    // [where user1 is the username]
    const fileName = `${user}-whatsappSession.json`;
    // Generate the path for the Session file based on the OS
    // ex linux: /home/user/Desktop/whatsapp-bot/sessions/user1-whatsappSession.json
    // windows: C:\Users\user\Desktop\whatsapp-bot\sessions\user1-whatsappSession.json
    const SESSION_FILE_PATH = `${logRootPath()}/${fileName}`;

    /* ----------------------------------------------------------------------
     Variables decelerations
     ---------------------------------------------------------------------- */
    // var to hold session config object.
    let sessionConfig;
    // var to hold path for the image if supplied.
    const media = () => {
      if (image) return `${logRootPath()}/temp/media/${user}/${image}`;
      if (video) return `${logRootPath()}/temp/media/${user}/${video}`;
      return undefined;
    };
    const mediaPath = media();
    // var to check if the client is ready and initialized.
    let isInitialized = false;
    // Array to store the contact if sent successfully
    let sentTo = [];
    // Array to store the contact if invalid.
    let notExistsContacts = [];
    /* ----------------------------------------------------------------------*/

    // check if the session file exists by syncing the file system.
    if (fs.existsSync(SESSION_FILE_PATH)) {
      // if the file sync returns true, import the file to the sessionConfig variable.
      sessionConfig = require(SESSION_FILE_PATH);
    }

    // create client object to run headless browser instance.
    const client = createClient(sessionConfig);

    // listen for the QR code if the selector occurs on the window;
    client.on('qr', (qr) => {
      // log the QR code string.
      console.log('QR Code RECEIVED', qr);
      // Generate the qr code from qr code string to be able to scan.
      qrcode.generate(qr, { small: true });
      // log the function call.
      writeFun('QR code Gen', user);
    });

    // listen for the authenticated event [done by QR code or previous session file stored locally ]
    client.on('authenticated', async (session) => {
      // whatsapp keeps on changing the session object.
      // Every time user logs in, recollect the session object to sessionConfig object
      sessionConfig = session;
      // Write the new Session values to the file, and if the file doesn't exists create one by the user name
      fs.writeFileSync(
        // path to the file
        SESSION_FILE_PATH,
        // the session object.
        JSON.stringify(sessionConfig),
        // callback function
        (err) => {
          if (err) {
            console.error(err);
          }
        }
      );
      // If user sends a image url string, download the image and save it to the imagePath variable.
      // if (image) {
      //   imagePath = await downloadImage(image, user, `${user}-1.png`);
      // }
      // log the function call.
      writeFun('authenticated', user);
    });

    // if the client is ready to send messages.
    client.on('ready', () => {
      // log the function call.
      writeFun('Ready to Pair', user);
    });

    // initialize the client.
    await client.initialize().then(() => {
      // set the initialized flag to true.
      isInitialized = true;
      // log the function call.
      writeFun('initialize', user);
    });
    // Read
    const arrayOfNumbers = readAndParseXLSX();
    // create a object of content to be sent.
    const data = { text, image, video, event, user, date };
    // if the client is successfully initialized, perform the operation to send message.
    if (isInitialized == true) {
      // initialize the Timer
      console.time('test');
      // loop into the numbers and perform the sendMessage operation.
      for (let i = 0; i < arrayOfNumbers.length; i++) {
        // remove useless units from number eg: space..
        const sanitized_number = arrayOfNumbers[i].Number.toString().replace(
          /[- )(]/g,
          ''
        );
        // add 91 to the sanitized number.
        const final_number = `91${sanitized_number.substring(
          sanitized_number.length - 10
        )}`;
        //send the number to the client and get the users number id only valid whatsapp user will have a id
        // the id object looks like-> id: 91955737xxxx@c.us [@c.us represents the server location ie: us servers]
        const numberRes = await client.getNumberId(final_number);
        // if the numberRes exists ie, the phone number is a valid one.
        if (numberRes) {
          // send the _serialized number to the send Message function. {_serialized number = 91xxxxxxxxxx@c.us}
          const sentRes = await sendTextMessage(
            client,
            numberRes._serialized,
            data,
            arrayOfNumbers[i].userId.toString()
          );
          // if the sentRes is true, push the number to sentTo array. [Success]
          if (sentRes == true) {
            sentTo.push(`Sent to ${arrayOfNumbers[i].Number}`);
            writeUserSuccess(`sent to ${arrayOfNumbers[i].Number}`, user);
          }
        } else if (!numberRes) {
          // if the number doesn't exists in the whatsapp servers, push it to array of invalid numbers..
          notExistsContacts.push(`${arrayOfNumbers[i].Number}`);
          writeUserError(
            `contact does not exists ${arrayOfNumbers[i].Number}`,
            user
          );
        }
      }
    }

    //End the Timer
    console.timeEnd('test');
    // Copy Every element from array storing the numbers that were sent successfully sent Message and write onto the log file of the particular user.
    // bulkWrite(sentTo, user, 'success');
    // Copy Every element from array storing the numbers that were not sent successfully sent Message and write onto the log file of the particular user.
    // bulkWrite(notExistsContacts, user, 'fail');
    // write the numbers of messages sent and by the user to the log file.
    writeSuccess(sentTo.length, user);
    // Destroy the client to avoid creation of a new browser window on each api hit.
    client.destroy();
    // send the response to the client and the list of numbers sent.
    res.status(200).json({
      sent: `${sentTo}`,
      sentTo: `${sentTo.length}`,
    });
  } catch (err) {
    writeUserError('controllers/sendMessage' + err, 'general');
    console.log(err);
    res.status(400).send(err);
    return;
  }
};
