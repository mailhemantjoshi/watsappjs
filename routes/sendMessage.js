const express = require('express');
const { sendMessageController } = require('../controllers/sendMessage');
const { sendMessageMiddleware } = require('../middleware/sendMessage');
const router = express.Router();

router.post('/sendMessage', sendMessageMiddleware, sendMessageController);

module.exports = router;
